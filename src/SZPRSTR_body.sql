--20180928 added for all restriction v_aumode and execute immediate to move between rollback and commit. 
---- rla 20201020 added use of lfst code where needed, and nvl -- NVL(a.scrrmaj_lfst_code,'X') #SR-888168288]
--- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
create or replace PACKAGE BODY EWUAPP.SZPRSTR AS
-- szprstr 1.0 


-------------------------CLASS---------------------------
-- 9.0 rla 20180925 CLASS SCRRCLS, SSRRCLS
---------------------------------------------------------
		procedure p_class_roll (	
			i_lis_file		IN UTL_FILE.FILE_TYPE,
			i_semester		IN VARCHAR2, 	
			i_quarter 		IN VARCHAR2, 
			i_aumode		IN VARCHAR2)
			AS

		  v_qtr_term ssbsect.ssbsect_term_code%TYPE;
		  v_sem_term ssbsect.ssbsect_term_code%TYPE;
		  v_aumode varchar2(9);
		 
			-- loop through terms at least one or more, at most two. {one quarter term, one semester term }
		  
		 Cursor t_term_res IS
		  SELECT a.stvterm_code, 
				 a.stvterm_trmt_code
			FROM STVTERM a
		   WHERE a.stvterm_code IN (v_qtr_term,v_sem_term)
			 and a.stvterm_code not in ('000000','999999');
			
		  ---------------------------------------------
		  CURSOR c_section_res(term_in1 varchar2) IS
				  SELECT b.ssbsect_term_code,
						 b.ssbsect_crn,       
						 b.ssbsect_subj_code,
						 b.ssbsect_crse_numb 
					FROM SSBSECT b
					--- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
				   WHERE b.ssbsect_term_code = term_in1
				     AND b.ssbsect_crn not in (select ssrattr_crn from ssrattr s where s.ssrattr_attr_code = 'PRSH'
                     AND s.ssrattr_term_code = b.ssbsect_term_code and s.ssrattr_crn = b.ssbsect_crn);
			   
			   
		-----------------------------------------------
		  CURSOR c_catalog_res (subj_in varchar2, crse_in varchar2, term_in2 varchar2, trmt_in varchar2) IS
			select a.SCRRCLS_subj_code,
				 a.SCRRCLS_crse_numb,
				 --- eff term 
				 a.SCRRCLS_eff_term,
				 --- from term 
				 a.scrrcls_eff_term from_term,                                                  
			-- to term, check to see if from term is equal to_to_term (subtract = 0)  if so make 999999, otherwise use  standard logic. 
			CASE WHEN substr((
					 SELECT max(f.scrrcls_eff_term)     
					 FROM scrrcls f 
					 WHERE f.scrrcls_subj_code = a.scrrcls_subj_code 
					 AND f.scrrcls_crse_numb = a.scrrcls_crse_numb)-a.scrrcls_eff_term,1,6)= '0' 
					 --------------
					 THEN '999999'
				  WHEN substr((
					 SELECT max(f.scrrcls_eff_term)     
					 FROM scrrcls f 
					 WHERE f.scrrcls_subj_code = a.scrrcls_subj_code 
					 AND f.scrrcls_crse_numb = a.scrrcls_crse_numb)-a.scrrcls_eff_term,1,6)<> '0'
					 -------------- 
					 THEN        (select max(f.scrrcls_eff_term) from scrrcls f 
										   where f.scrrcls_subj_code = a.scrrcls_subj_code 
											 and f.scrrcls_crse_numb = a.scrrcls_crse_numb)
					 END to_term,
				  --- 
				 a.SCRRCLS_rec_type,
				 a.SCRRCLS_class_ind,
				 a.SCRRCLS_clas_code,
				 a.SCRRCLS_activity_date
			from SCRRCLS a
			where a.SCRRCLS_eff_term =  (select max(b.SCRRCLS_eff_term) 
												from SCRRCLS b
											   where b.SCRRCLS_eff_term = a.SCRRCLS_eff_term 
											   and b.SCRRCLS_subj_code = a.SCRRCLS_subj_code
											   and b.SCRRCLS_crse_numb = a.SCRRCLS_crse_numb
											   and b.SCRRCLS_eff_term in (select stvterm_code from stvterm 
																		  where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
											 group by b.SCRRCLS_subj_code, b.SCRRCLS_crse_numb)
				and (select scbcrky.scbcrky_term_code_end 
				from scbcrky 
			   where scbcrky.scbcrky_subj_code = a.scrrcls_subj_code
				 and scbcrky.scbcrky_crse_numb = a.scrrcls_crse_numb) = '999999'
				 --
				and a.SCRRCLS_subj_code = subj_in 
				and a.SCRRCLS_crse_numb = crse_in 
				and a.SCRRCLS_eff_term <= term_in2 
				and a.SCRRCLS_eff_term in (select stvterm_code from stvterm 
										   where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
		   order by a.SCRRCLS_subj_code, 
					a.SCRRCLS_crse_numb, 
					a.SCRRCLS_rec_type;
			
		BEGIN
		
			IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;

		  
		   UTL_FILE.PUT_LINE(i_lis_file,'ROLL CLASS RESTRICTIONS');  
       UTL_FILE.PUT_LINE(i_lis_file,' '); 

		v_qtr_term := i_quarter; 
		v_sem_term := i_semester;

		FOR t_term_rec IN t_term_res LOOP
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_code);
		   UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_trmt_code);
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 
			 delete from ssrrcls where ssrrcls_term_code = t_term_rec.stvterm_code;
			 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;'); 
			

			   FOR r_section_rec IN c_section_res(t_term_rec.stvterm_code) LOOP    
				 UTL_FILE.PUT_LINE(i_lis_file,r_section_rec.ssbsect_term_code||','||r_section_rec.ssbsect_subj_code||','||r_section_rec.ssbsect_crse_numb||','||r_section_rec.ssbsect_crn);
					  
					FOR r_catalog_rec in c_catalog_res(r_section_rec.ssbsect_subj_code,r_section_rec.ssbsect_crse_numb,r_section_rec.ssbsect_term_code, t_term_rec.stvterm_trmt_code) LOOP
						
					 IF r_catalog_rec.SCRRCLS_rec_type is not null 
					 THEN        
								
					 
					 
						if r_catalog_rec.to_term <= t_term_rec.stvterm_code
						then 
							 
							  UTL_FILE.PUT_LINE(i_lis_file,'SKIP RECORD:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
									  's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
															  
									  's_CRN'||','||r_section_rec.ssbsect_crn||','||
									  'c_rectype'||','||r_catalog_rec.SCRRCLS_rec_type||','||
									  'c_class_ind'||','||r_catalog_rec.SCRRCLS_class_ind||','||
									  'c_clas_code'||','||r_catalog_rec.SCRRCLS_clas_code||','||
									  'date'||','||sysdate);
						else 
						  
							   UTL_FILE.PUT_LINE(i_lis_file,'     '||'Inserting:'||' '||'from term'||','||
							   r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
									  's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
									  's_CRN'||','||r_section_rec.ssbsect_crn||','||
									  'c_rectype'||','||r_catalog_rec.SCRRCLS_rec_type||','||
									  'c_class_ind'||','||r_catalog_rec.SCRRCLS_class_ind||','||
									  'c_clas_code'||','||r_catalog_rec.SCRRCLS_clas_code||','||
									  'date'||','||sysdate);
									  
									  
											   BEGIN        
									  INSERT INTO saturn.ssrrcls(
											  ssrrcls_term_code,
											  ssrrcls_crn,
											  ssrrcls_rec_type,
											  ssrrcls_class_ind,
											  ssrrcls_clas_code,
											  ssrrcls_activity_date
										  )
										  VALUES
										  (   r_section_rec.ssbsect_term_code,
											  r_section_rec.ssbsect_crn,
											  r_catalog_rec.SCRRCLS_rec_type,
											  r_catalog_rec.SCRRCLS_class_ind,
											  r_catalog_rec.SCRRCLS_clas_code,
											  sysdate);
										  EXCEPTION
										  WHEN OTHERS THEN
										  UTL_FILE.PUT_LINE(i_lis_file,' ----- ' || sqlerrm);
							   END;
							   
							   EXECUTE IMMEDIATE ('begin '||v_aumode||' end;'); 


									 
						 end if;
						 
					 -- rla added ELSIF 20180911           
					 ELSIF  r_catalog_rec.SCRRCLS_rec_type is null THEN
					 UTL_FILE.PUT_LINE(i_lis_file,'no data found skip records');				
					 
					 END IF;
					 
					END LOOP;
			   END LOOP;
		END LOOP;

			  -- End of Report Clean up
			UTL_FILE.PUT_LINE(i_lis_file,' CLASS completed successfully  ');
			--do I need this here? UTL_FILE.fclose(i_lis_file);
		  
		END;

-------------------------DEGREE----------------------------------------------
-- 9.0 rla 20180925 --DEGREE --SCRRDEG, SSRRDEG
-----------------------------------------------------------------------------
		procedure p_degree_roll (	
			i_lis_file		IN UTL_FILE.FILE_TYPE,
			i_semester		IN VARCHAR2, 	
			i_quarter 		IN VARCHAR2, 
			i_aumode		IN VARCHAR2)
			AS

		  v_qtr_term ssbsect.ssbsect_term_code%TYPE;
		  v_sem_term ssbsect.ssbsect_term_code%TYPE;
		  v_aumode varchar2(9);
		  
			-- loop through terms at least one or more, at most two. {one quarter term, one semester term }
		  
		 Cursor t_term_res IS
		  SELECT a.stvterm_code, 
				 a.stvterm_trmt_code
			FROM STVTERM a
		   WHERE a.stvterm_code IN (v_qtr_term,v_sem_term)
			 and a.stvterm_code not in ('000000','999999');
		  
		  
		  ---------------------------------------------
		  CURSOR c_section_res(term_in1 varchar2) IS
				  SELECT b.ssbsect_term_code,
						 b.ssbsect_crn,       
						 b.ssbsect_subj_code,
						 b.ssbsect_crse_numb 
					FROM SSBSECT b
				   WHERE b.ssbsect_term_code = term_in1					
				   --- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
				     AND b.ssbsect_crn not in (select ssrattr_crn from ssrattr s where s.ssrattr_attr_code = 'PRSH'
                     AND s.ssrattr_term_code = b.ssbsect_term_code and s.ssrattr_crn = b.ssbsect_crn);
		-----------------------------------------------
		  CURSOR c_catalog_res (subj_in varchar2, crse_in varchar2, term_in2 varchar2, trmt_in varchar2) IS
			select a.scrrdeg_subj_code,
				 a.scrrdeg_crse_numb,
				 -- eff term
				 a.scrrdeg_term_code_effective,
				 -- from term
				 a.scrrdeg_term_code_effective from_term,
				 -- to term, check to see if from term is equal to_to_term (subtract = 0)  if so make 999999, otherwise use  standard logic. 
			CASE WHEN substr((
					 SELECT max(f.scrrdeg_term_code_effective)     
					 FROM scrrdeg f 
					 WHERE f.scrrdeg_subj_code = a.scrrdeg_subj_code 
					 AND f.scrrdeg_crse_numb = a.scrrdeg_crse_numb)-a.scrrdeg_term_code_effective,1,6)= '0' 
					 --------------
					 THEN '999999'
				  WHEN substr((
					 SELECT max(f.scrrdeg_term_code_effective)     
					 FROM scrrdeg f 
					 WHERE f.scrrdeg_subj_code = a.scrrdeg_subj_code 
					 AND f.scrrdeg_crse_numb = a.scrrdeg_crse_numb)-a.scrrdeg_term_code_effective,1,6)<> '0'
					 -------------- 
					 THEN        (select max(f.scrrdeg_term_code_effective) from scrrdeg f 
										   where f.scrrdeg_subj_code = a.scrrdeg_subj_code 
											 and f.scrrdeg_crse_numb = a.scrrdeg_crse_numb)
					 END to_term,		 
				 --
				 a.scrrdeg_rec_type,
				 a.scrrdeg_degc_ind,
				 a.scrrdeg_degc_code,
				 a.scrrdeg_activity_date
			from scrrdeg a
			where a.scrrdeg_term_code_effective =  (select max(b.scrrdeg_term_code_effective) 
												from scrrdeg b
											   where b.scrrdeg_term_code_effective = a.scrrdeg_term_code_effective 
											   and b.scrrdeg_subj_code = a.scrrdeg_subj_code
											   and b.scrrdeg_crse_numb = a.scrrdeg_crse_numb
											   and b.scrrdeg_term_code_effective in (select stvterm_code from stvterm 
																					  where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)									   
											 group by b.scrrdeg_subj_code, b.scrrdeg_crse_numb)
													   and (select scbcrky.scbcrky_term_code_end 
				from scbcrky 
			   where scbcrky.scbcrky_subj_code = a.scrrdeg_subj_code
				 and scbcrky.scbcrky_crse_numb = a.scrrdeg_crse_numb) = '999999'
				 ----------
				and a.scrrdeg_subj_code = subj_in 
				and a.scrrdeg_crse_numb = crse_in 
				and a.scrrdeg_term_code_effective <= term_in2 
				and a.scrrdeg_term_code_effective in (select stvterm_code from stvterm 
													   where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)			
		   order by a.scrrdeg_subj_code, 
					a.scrrdeg_crse_numb, 
					a.scrrdeg_rec_type;
			
		BEGIN
		
			IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;
 
		UTL_FILE.PUT_LINE(i_lis_file,'ROLL DEGREE RESTRICTIONS'); 
    UTL_FILE.PUT_LINE(i_lis_file,' '); 

		v_qtr_term := i_quarter;
		v_sem_term := i_semester;

		FOR t_term_rec IN t_term_res LOOP
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_code);
				 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_trmt_code);
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 
			 delete from ssrrdeg where ssrrdeg_term_code = t_term_rec.stvterm_code;
			 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
			 

			   FOR r_section_rec IN c_section_res(t_term_rec.stvterm_code) LOOP    
				   UTL_FILE.PUT_LINE(i_lis_file,r_section_rec.ssbsect_term_code||','||r_section_rec.ssbsect_subj_code||','||r_section_rec.ssbsect_crse_numb||','||r_section_rec.ssbsect_crn);
					  
					FOR r_catalog_rec in c_catalog_res(r_section_rec.ssbsect_subj_code,r_section_rec.ssbsect_crse_numb,r_section_rec.ssbsect_term_code, t_term_rec.stvterm_trmt_code) LOOP
						
					 IF r_catalog_rec.scrrdeg_rec_type is not null 
					 THEN
					 					 
						if r_catalog_rec.to_term <= t_term_rec.stvterm_code 
					 
						then        UTL_FILE.PUT_LINE(i_lis_file,'SKIP RECORD:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','|| 
								's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
								's_CRN'||','||r_section_rec.ssbsect_crn||','||
								'c_rectype'||','||r_catalog_rec.scrrdeg_rec_type||','||
								'c_degc_ind'||','||r_catalog_rec.scrrdeg_degc_ind||','||
								'c_degc_code'||','||r_catalog_rec.scrrdeg_degc_code||','||
								'date'||','||sysdate);
					 
						else 
								UTL_FILE.PUT_LINE(i_lis_file,'     '||'Inserting:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||        
								's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
								's_CRN'||','||r_section_rec.ssbsect_crn||','||
								'c_rectype'||','||r_catalog_rec.scrrdeg_rec_type||','||
								'c_degc_ind'||','||r_catalog_rec.scrrdeg_degc_ind||','||
								'c_degc_code'||','||r_catalog_rec.scrrdeg_degc_code||','||
								'date'||','||sysdate);
								
								 BEGIN        
										INSERT INTO ssrrdeg(
												ssrrdeg_term_code,
												ssrrdeg_crn,
												ssrrdeg_rec_type,
												ssrrdeg_degc_ind,
												ssrrdeg_degc_code,
												ssrrdeg_activity_date
											)
											VALUES
											(   r_section_rec.ssbsect_term_code,
												r_section_rec.ssbsect_crn,
												r_catalog_rec.scrrdeg_rec_type,
												r_catalog_rec.scrrdeg_degc_ind,
												r_catalog_rec.scrrdeg_degc_code,
												sysdate);
											EXCEPTION
											WHEN OTHERS THEN
											UTL_FILE.PUT_LINE(i_lis_file,' ----- ' || sqlerrm);
								 END;
								 
								 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
										
						  end if;
							  
					   ELSIF  r_catalog_rec.scrrdeg_rec_type is null THEN
					   UTL_FILE.PUT_LINE(i_lis_file,'no data found skip records');
					   
					   END IF;      
					END LOOP;
			   END LOOP;
		END LOOP;

			  -- End of Report Clean up
			UTL_FILE.PUT_LINE(i_lis_file,' DEGREE completed successfully  ');
		  
		END;
		
---------------------------------------------------------------------------------------------------
--- EWUAPP IN DEVL HAS INSUFFICIENT PRIVILEGES 20180925 2:33PM SCRRDEP, SSRRDEP, ssrrdep_pk_sequence.nextval
--441/17   PL/SQL: ORA-01031: insufficient privileges
--441/5    PL/SQL: SQL Statement ignored
--484/19   PL/SQL: ORA-02289: sequence does not exist
--473/14   PL/SQL: SQL Statement ignored

------------------------------DEPARTMENT-----------------
-- 9.0 rla 20180925  DEPT SCRRDEP, SSRRDEP
---------------------------------------------------------

		procedure p_department_roll (	
			i_lis_file	IN UTL_FILE.FILE_TYPE,
			i_semester		IN VARCHAR2, 	
			i_quarter 		IN VARCHAR2, 
			i_aumode		IN VARCHAR2)
			AS

		  v_qtr_term ssbsect.ssbsect_term_code%TYPE;
		  v_sem_term ssbsect.ssbsect_term_code%TYPE;
		  v_aumode varchar2(9);
		  
		  -- loop through terms at least one or more, at most two. {one quarter term, one semester term }
		  
		 Cursor t_term_res IS
		  SELECT a.stvterm_code, 
				 a.stvterm_trmt_code
			FROM STVTERM a
		   WHERE a.stvterm_code IN (v_qtr_term,v_sem_term)
			 and a.stvterm_code not in ('000000','999999');
		  
		  ---------------------------------------------  
		  CURSOR c_section_res(term_in1 varchar2) IS
				  SELECT b.ssbsect_term_code,
						 b.ssbsect_crn,       
						 b.ssbsect_subj_code,
						 b.ssbsect_crse_numb 
					FROM SSBSECT b
				   WHERE b.ssbsect_term_code = term_in1
						--- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
				     AND b.ssbsect_crn not in (select ssrattr_crn from ssrattr s where s.ssrattr_attr_code = 'PRSH'
                     AND s.ssrattr_term_code = b.ssbsect_term_code and s.ssrattr_crn = b.ssbsect_crn);  				   
		-----------------------------------------------
		  CURSOR c_catalog_res (subj_in varchar2, crse_in varchar2, term_in2 varchar2, trmt_in varchar2) IS
			select a.scrrdep_subj_code,
				 a.scrrdep_crse_numb,
				 -- eff term 
				 a.scrrdep_eff_term,
				 -- from term
				 a.scrrdep_eff_term from_term,
				 -- to term, check to see if from term is equal to_to_term (subtract = 0)  if so make 999999, otherwise use  standard logic. 
			CASE WHEN substr((
					 SELECT max(f.scrrdep_eff_term)     
					 FROM scrrdep f 
					 WHERE f.scrrdep_subj_code = a.scrrdep_subj_code 
					 AND f.scrrdep_crse_numb = a.scrrdep_crse_numb)-a.scrrdep_eff_term,1,6)= '0' 
					 --------------
					 THEN '999999'
				  WHEN substr((
					 SELECT max(f.scrrdep_eff_term)     
					 FROM scrrdep f 
					 WHERE f.scrrdep_subj_code = a.scrrdep_subj_code 
					 AND f.scrrdep_crse_numb = a.scrrdep_crse_numb)-a.scrrdep_eff_term,1,6)<> '0'
					 -------------- 
					 THEN        (select max(f.scrrdep_eff_term) from scrrdep f 
										   where f.scrrdep_subj_code = a.scrrdep_subj_code 
											 and f.scrrdep_crse_numb = a.scrrdep_crse_numb)
					 END to_term,		 
				 --
				 a.scrrdep_rec_type,
				 a.scrrdep_dept_ie_cde,
				 a.scrrdep_dept_code,
				 a.scrrdep_activity_date
			from scrrdep a
			where a.scrrdep_eff_term =  (select max(b.scrrdep_eff_term) 
												from scrrdep b
											   where b.scrrdep_eff_term = a.scrrdep_eff_term 
											   and b.scrrdep_subj_code = a.scrrdep_subj_code
											   and b.scrrdep_crse_numb = a.scrrdep_crse_numb
											   and b.scrrdep_eff_term in (select stvterm_code from stvterm 
																		  where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
											 group by b.scrrdep_subj_code, b.scrrdep_crse_numb)
				 and (select scbcrky.scbcrky_term_code_end 
				from scbcrky 
			   where scbcrky.scbcrky_subj_code = a.scrrdep_subj_code
				 and scbcrky.scbcrky_crse_numb = a.scrrdep_crse_numb) = '999999'
				and a.scrrdep_subj_code = subj_in
				and a.scrrdep_crse_numb = crse_in 
				and a.scrrdep_eff_term <= term_in2 
				and a.scrrdep_eff_term in (select stvterm_code from stvterm 
										   where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
		   order by a.scrrdep_subj_code, 
					a.scrrdep_crse_numb, 
					a.scrrdep_rec_type;
			
		BEGIN
		
			IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;
		  
		UTL_FILE.PUT_LINE(i_lis_file,'ROLL DEPARTMENT RESTRICTIONS');  
    UTL_FILE.PUT_LINE(i_lis_file,' '); 

		v_qtr_term := i_quarter;
		v_sem_term := i_semester;

		FOR t_term_rec IN t_term_res LOOP
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_code);
			 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_trmt_code);
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 
			 delete from ssrrdep where ssrrdep_term_code = t_term_rec.stvterm_code;
			 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
			 

			   FOR r_section_rec IN c_section_res(t_term_rec.stvterm_code) LOOP    
				UTL_FILE.PUT_LINE(i_lis_file,r_section_rec.ssbsect_term_code||','||r_section_rec.ssbsect_subj_code||','||r_section_rec.ssbsect_crse_numb||','||r_section_rec.ssbsect_crn);
					  
					FOR r_catalog_rec in c_catalog_res(r_section_rec.ssbsect_subj_code,r_section_rec.ssbsect_crse_numb,r_section_rec.ssbsect_term_code, t_term_rec.stvterm_trmt_code ) LOOP

					 IF r_catalog_rec.scrrdep_rec_type is not null 
					 THEN
										
								
								if r_catalog_rec.to_term <= t_term_rec.stvterm_code 
								then
											UTL_FILE.PUT_LINE(i_lis_file,'SKIP RECORD:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
											's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
											's_CRN'||','||r_section_rec.ssbsect_crn||','||
											'c_rectype'||','||r_catalog_rec.scrrdep_rec_type||','||
											'c_dept_ie_cde'||','||r_catalog_rec.scrrdep_dept_ie_cde||','||
											'c_dept_code'||','||r_catalog_rec.scrrdep_dept_code||','||
											'date'||','||sysdate);
														
								else						
											UTL_FILE.PUT_LINE(i_lis_file,'     '||'Inserting:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||  
											's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
											's_CRN'||','||r_section_rec.ssbsect_crn||','||
											'c_rectype'||','||r_catalog_rec.scrrdep_rec_type||','||
											'c_dept_ie_cde'||','||r_catalog_rec.scrrdep_dept_ie_cde||','||
											'c_dept_code'||','||r_catalog_rec.scrrdep_dept_code||','||
											'date'||','||sysdate);
											
											  BEGIN      
													INSERT INTO saturn.ssrrdep(
															ssrrdep_primary_key,
															ssrrdep_user_id,
															ssrrdep_term_code,
															ssrrdep_crn,
															ssrrdep_rec_type,
															ssrrdep_dept_ie_cde,
															ssrrdep_dept_code,
															ssrrdep_activity_date
														)
														VALUES
														(   ssrrdep_pk_sequence.nextval,
															USER,
															r_section_rec.ssbsect_term_code,
															r_section_rec.ssbsect_crn,
															r_catalog_rec.scrrdep_rec_type,
															r_catalog_rec.scrrdep_dept_ie_cde,
															r_catalog_rec.scrrdep_dept_code,
															sysdate);
														EXCEPTION
														WHEN OTHERS THEN
														UTL_FILE.PUT_LINE(i_lis_file,' ----- ' || sqlerrm);
											 END;
											 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
											
								end if;
										  
						ELSIF  r_catalog_rec.scrrdep_rec_type is null THEN
						UTL_FILE.PUT_LINE(i_lis_file,'no data found skip records');
						
						-----ELSE            
							 -----UTL_FILE.PUT_LINE(i_lis_file,'ELSE');
					   
						END IF;      
					END LOOP;
			   END LOOP;
		END LOOP;

			  -- End of Report Clean up
			UTL_FILE.PUT_LINE(i_lis_file,' DEPARTMENT completed successfully  ');

		END;

-----------------------------------------------------------------------------------------------------------------------------		


---------------------------------------------LEVEL ---------------------------
-- 9.0 rla 20180925 level SCRRLVL, SSRRLVL
-----------------------------------------------------------------------------

		procedure p_level_roll (
			i_lis_file	IN UTL_FILE.FILE_TYPE,
			i_semester		IN VARCHAR2, 	
			i_quarter 		IN VARCHAR2, 
			i_aumode		IN VARCHAR2)
			AS

		  v_qtr_term ssbsect.ssbsect_term_code%TYPE;
		  v_sem_term ssbsect.ssbsect_term_code%TYPE;
		  v_aumode varchar2(9);
		  
			-- loop through terms at least one or more, at most two. {one quarter term, one semester term }
		 Cursor t_term_res IS
		  SELECT a.stvterm_code, 
				 a.stvterm_trmt_code
			FROM STVTERM a
		   WHERE a.stvterm_code IN (v_qtr_term,v_sem_term)
			 and a.stvterm_code not in ('000000','999999');
			 
		  ---------------------------------------------
		  CURSOR c_section_res(term_in1 varchar2) IS
				  SELECT b.ssbsect_term_code,
						 b.ssbsect_crn,       
						 b.ssbsect_subj_code,
						 b.ssbsect_crse_numb 
					FROM SSBSECT b
				   WHERE b.ssbsect_term_code = term_in1
				   	--- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
				     AND b.ssbsect_crn not in (select ssrattr_crn from ssrattr s where s.ssrattr_attr_code = 'PRSH'
                     AND s.ssrattr_term_code = b.ssbsect_term_code and s.ssrattr_crn = b.ssbsect_crn);
		-----------------------------------------------
		  CURSOR c_catalog_res (subj_in varchar2, crse_in varchar2, term_in2 varchar2, trmt_in varchar2) IS
			select a.SCRRLVL_subj_code,
				 a.SCRRLVL_crse_numb,
				 -- eff term 
				 a.SCRRLVL_eff_term,
				 -- from term
				 a.SCRRLVL_eff_term from_term,
				 -- to term, check to see if from term is equal to_to_term (subtract = 0)  if so make 999999, otherwise use  standard logic. 
			CASE WHEN substr((
					 SELECT max(f.SCRRLVL_eff_term)     
					 FROM SCRRLVL f 
					 WHERE f.SCRRLVL_subj_code = a.SCRRLVL_subj_code 
					 AND f.SCRRLVL_crse_numb = a.SCRRLVL_crse_numb)-a.SCRRLVL_eff_term,1,6)= '0' 
					 --------------
					 THEN '999999'
				  WHEN substr((
					 SELECT max(f.SCRRLVL_eff_term)     
					 FROM SCRRLVL f 
					 WHERE f.SCRRLVL_subj_code = a.SCRRLVL_subj_code 
					 AND f.SCRRLVL_crse_numb = a.SCRRLVL_crse_numb)-a.SCRRLVL_eff_term,1,6)<> '0'
					 -------------- 
					 THEN        (select max(f.SCRRLVL_eff_term) from SCRRLVL f 
										   where f.SCRRLVL_subj_code = a.SCRRLVL_subj_code 
											 and f.SCRRLVL_crse_numb = a.SCRRLVL_crse_numb)
					 END to_term,		 
				 --

				 a.SCRRLVL_rec_type,
				 a.SCRRLVL_levl_ind,
				 a.SCRRLVL_levl_code,
				 a.SCRRLVL_activity_date
			from SCRRLVL a
			where a.SCRRLVL_eff_term =  (select max(b.SCRRLVL_eff_term) 
												from SCRRLVL b
											   where b.SCRRLVL_eff_term = a.SCRRLVL_eff_term 
											   and b.SCRRLVL_subj_code = a.SCRRLVL_subj_code
											   and b.SCRRLVL_crse_numb = a.SCRRLVL_crse_numb
											   and b.SCRRLVL_eff_term in (select stvterm_code from stvterm 
																		  where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
											 group by b.SCRRLVL_subj_code, b.SCRRLVL_crse_numb)
				and (select scbcrky.scbcrky_term_code_end 
				from scbcrky 
			   where scbcrky.scbcrky_subj_code = a.scrrlvl_subj_code
				 and scbcrky.scbcrky_crse_numb = a.scrrlvl_crse_numb) = '999999'
				 --------
				and a.SCRRLVL_subj_code = subj_in  
				and a.SCRRLVL_crse_numb = crse_in 
				and a.SCRRLVL_eff_term <= term_in2 
				and a.SCRRLVL_eff_term in (select stvterm_code from stvterm 
											where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
		   order by a.SCRRLVL_subj_code, 
					a.SCRRLVL_crse_numb, 
					a.SCRRLVL_rec_type;
			
		BEGIN
		
			IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;

		  
		UTL_FILE.PUT_LINE(i_lis_file,'ROLL LEVEL RESTRICTIONS');  
    UTL_FILE.PUT_LINE(i_lis_file,' '); 

		v_qtr_term := i_quarter;
		v_sem_term := i_semester;

		FOR t_term_rec IN t_term_res LOOP
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_code);
				 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_trmt_code);
			 UTL_FILE.PUT_LINE(i_lis_file,'');
			 
			 delete from ssrrlvl where ssrrlvl_term_code = t_term_rec.stvterm_code;
			 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
			 

			   FOR r_section_rec IN c_section_res(t_term_rec.stvterm_code) LOOP    
				 UTL_FILE.PUT_LINE(i_lis_file,r_section_rec.ssbsect_term_code||','||r_section_rec.ssbsect_subj_code||','||r_section_rec.ssbsect_crse_numb||','||r_section_rec.ssbsect_crn);
					  
					FOR r_catalog_rec in c_catalog_res(r_section_rec.ssbsect_subj_code,r_section_rec.ssbsect_crse_numb,r_section_rec.ssbsect_term_code, t_term_rec.stvterm_trmt_code) LOOP
						
					 IF r_catalog_rec.SCRRLVL_rec_type is not null 
					 THEN
					 
					 
						   if r_catalog_rec.to_term <= t_term_rec.stvterm_code 
						   then
						
										UTL_FILE.PUT_LINE(i_lis_file,'SKIP RECORD:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
										's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
										's_CRN'||','||r_section_rec.ssbsect_crn||','||
										'c_rectype'||','||r_catalog_rec.SCRRLVL_rec_type||','||
										'c_levl_ind'||','||r_catalog_rec.SCRRLVL_levl_ind||','||
										'c_levl_code'||','||r_catalog_rec.SCRRLVL_levl_code||','||
										'date'||','||sysdate);

						   else 
										
										UTL_FILE.PUT_LINE(i_lis_file,'     '||'Inserting:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
										's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
										's_CRN'||','||r_section_rec.ssbsect_crn||','||
										'c_rectype'||','||r_catalog_rec.SCRRLVL_rec_type||','||
										'c_levl_ind'||','||r_catalog_rec.SCRRLVL_levl_ind||','||
										'c_levl_code'||','||r_catalog_rec.SCRRLVL_levl_code||','||
										'date'||','||sysdate);
								 BEGIN      
										INSERT INTO saturn.ssrrlvl(
												ssrrlvl_term_code,
												ssrrlvl_crn,
												ssrrlvl_rec_type,
												ssrrlvl_levl_ind,
												ssrrlvl_levl_code,
												ssrrlvl_activity_date
											)
											VALUES
											(   r_section_rec.ssbsect_term_code,
												r_section_rec.ssbsect_crn,
												r_catalog_rec.SCRRLVL_rec_type,
												r_catalog_rec.SCRRLVL_levl_ind,
												r_catalog_rec.SCRRLVL_levl_code,
												sysdate);
											EXCEPTION
											WHEN OTHERS THEN
											UTL_FILE.PUT_LINE(i_lis_file,' ----- ' || sqlerrm);
								 END;
								 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
										
						   end if;
								 
						ELSIF  r_catalog_rec.SCRRLVL_rec_type is null THEN
						UTL_FILE.PUT_LINE(i_lis_file,'no data found skip records');
						
						END IF;      
					END LOOP;
			   END LOOP;
		END LOOP;

			  -- End of Report Clean up
			UTL_FILE.PUT_LINE(i_lis_file,' LEVEL completed successfully  ');
		  
		END;
	
	
	
-------------------------MAJOR-----------------------------------
-- 9.0 rla 20180925 MAJOR SCRRMAJ, SSRRMAJ
-----------------------------------------------------------------

procedure p_major_roll (
	i_lis_file	IN UTL_FILE.FILE_TYPE,
	i_semester		IN VARCHAR2, 	
	i_quarter 		IN VARCHAR2, 
	i_aumode		IN VARCHAR2)
	AS

  v_qtr_term ssbsect.ssbsect_term_code%TYPE;
  v_sem_term ssbsect.ssbsect_term_code%TYPE;
  v_aumode varchar2(9);
  
  v_flag varchar2(1) := '0';
 
    -- loop through terms at least one or more, at most two. {one quarter term, one semester term }
 Cursor t_term_res IS
  SELECT a.stvterm_code, 
         a.stvterm_trmt_code
    FROM STVTERM a
   WHERE a.stvterm_code IN (v_qtr_term,v_sem_term)
     and a.stvterm_code not in ('000000','999999');
	 
  ---------------------------------------------
  CURSOR c_section_res(term_in1 varchar2) IS
          SELECT b.ssbsect_term_code,
                 b.ssbsect_crn,       
                 b.ssbsect_subj_code,
                 b.ssbsect_crse_numb 
            FROM SSBSECT b
           WHERE b.ssbsect_term_code = term_in1
		   	--- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
		    AND b.ssbsect_crn not in (select ssrattr_crn from ssrattr s where s.ssrattr_attr_code = 'PRSH'
            AND s.ssrattr_term_code = b.ssbsect_term_code and s.ssrattr_crn = b.ssbsect_crn)
           ORDER BY b.SSBSECT_TERM_CODE,b.SSBSECT_SUBJ_CODE,b.SSBSECT_CRSE_NUMB,b.SSBSECT_CRN;
-----------------------------------------------
  CURSOR c_catalog_res (subj_in varchar2, crse_in varchar2, term_in2 varchar2, trmt_in varchar2) IS
    select a.scrrmaj_subj_code,
         a.scrrmaj_crse_numb,
		 -- eff term
         a.scrrmaj_eff_term,
		 -- from term
		 a.scrrmaj_eff_term from_term,
		 -- to term, check to see if from term is equal to_to_term (subtract = 0)  if so make 999999, 
		 --otherwise use  standard logic. 
    CASE WHEN substr((
             SELECT max(f.scrrmaj_eff_term)     
             FROM scrrmaj f 
             WHERE f.scrrmaj_subj_code = a.scrrmaj_subj_code 
             AND f.scrrmaj_crse_numb = a.scrrmaj_crse_numb
			 -- rla 20201020
			 and NVL(f.scrrmaj_lfst_code,'X') = NVL(a.scrrmaj_lfst_code,'X'))-a.scrrmaj_eff_term,1,6)= '0' 
             --------------
             THEN '999999'
          WHEN substr((
             SELECT max(f.scrrmaj_eff_term)     
             FROM scrrmaj f 
             WHERE f.scrrmaj_subj_code = a.scrrmaj_subj_code 
             AND f.scrrmaj_crse_numb = a.scrrmaj_crse_numb
			 -- rla 20201020
			 and NVL(f.scrrmaj_lfst_code,'X') = NVL(a.scrrmaj_lfst_code,'X'))-a.scrrmaj_eff_term,1,6)<> '0'
             -------------- 
             THEN        (select max(f.scrrmaj_eff_term) from scrrmaj f 
                                   where f.scrrmaj_subj_code = a.scrrmaj_subj_code 
                                     and f.scrrmaj_crse_numb = a.scrrmaj_crse_numb
									 -- rla 20201020
									 and NVL(f.scrrmaj_lfst_code,'X') = NVL(a.scrrmaj_lfst_code,'X'))
             END to_term,		 
		 --
         a.scrrmaj_rec_type,
         a.scrrmaj_major_ind,
         a.scrrmaj_majr_code,
         a.scrrmaj_lfst_code,
         a.scrrmaj_activity_date
    from scrrmaj a
    where a.scrrmaj_eff_term =  (select max(b.scrrmaj_eff_term) 
                                        from scrrmaj b
                                       where b.scrrmaj_eff_term = a.scrrmaj_eff_term 
                                       and b.scrrmaj_subj_code = a.scrrmaj_subj_code
                                       and b.scrrmaj_crse_numb = a.scrrmaj_crse_numb
									   -- rla 20201020
									   and NVL(b.scrrmaj_lfst_code,'X') = NVL(a.scrrmaj_lfst_code,'X')
                                       and b.scrrmaj_eff_term in (select stvterm_code from stvterm 
                                                                  where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
                                     group by b.scrrmaj_subj_code, b.scrrmaj_crse_numb)
          and (select scbcrky.scbcrky_term_code_end 
        from scbcrky 
       where scbcrky.scbcrky_subj_code = a.scrrmaj_subj_code
         and scbcrky.scbcrky_crse_numb = a.scrrmaj_crse_numb) = '999999'
         --------
        and a.scrrmaj_subj_code = subj_in 
        and a.scrrmaj_crse_numb = crse_in 
        and a.scrrmaj_eff_term <= term_in2 
        and a.scrrmaj_eff_term in (select stvterm_code from stvterm 
                                   where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
   order by a.scrrmaj_majr_code,
            a.scrrmaj_subj_code, 
            a.scrrmaj_crse_numb, 
            a.scrrmaj_rec_type;
    
BEGIN

			IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;
  
UTL_FILE.PUT_LINE(i_lis_file,'ROLL MAJOR RESTRICTIONS');  
UTL_FILE.PUT_LINE(i_lis_file,' ');  

v_sem_term := i_semester;
v_qtr_term := i_quarter;

--- term is loop 1 
FOR t_term_rec IN t_term_res LOOP
     UTL_FILE.PUT_LINE(i_lis_file,'');
     UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_code);
	 	 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_trmt_code);
     UTL_FILE.PUT_LINE(i_lis_file,'');
     
     delete from ssrrmaj where ssrrmaj_term_code = t_term_rec.stvterm_code;
	 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
     
	 
 
       FOR r_section_rec IN c_section_res(t_term_rec.stvterm_code) LOOP    
	   
	   -- list rla 
        UTL_FILE.PUT_LINE(i_lis_file,' SSBSECT '||' TERM '||r_section_rec.ssbsect_term_code||','||'SUBJ '||r_section_rec.ssbsect_subj_code||','||'CRSE '||r_section_rec.ssbsect_crse_numb||','||'CRN '||r_section_rec.ssbsect_crn);
		
            FOR r_catalog_rec in c_catalog_res(r_section_rec.ssbsect_subj_code,r_section_rec.ssbsect_crse_numb,r_section_rec.ssbsect_term_code, t_term_rec.stvterm_trmt_code) LOOP
                
            ------- for preq this value was [r_catalog_rec.scrrtst_seqno is not null] what will this be for scrrmaj restriction? 
 
             IF r_catalog_rec.scrrmaj_rec_type is not null 
			 THEN
                v_flag := '1';
				
				     ---- rla 20200912 tshoot
					 
					  UTL_FILE.PUT_LINE(i_lis_file, ' If CATALOG_REC.TO_TERM: '||r_catalog_rec.to_term||' <= '|| ' T_TERM_REC.STVTERM_CODE: '||t_term_rec.stvterm_code|| 'then skip, otherwise insert');
				
			        -- rla 20201029 add to_number()
					if to_number(r_catalog_rec.to_term) <= to_number(t_term_rec.stvterm_code) 
                    then	
					
						 
				                    UTL_FILE.PUT_LINE(i_lis_file,'Skip:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
				   					's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
									's_CRN'||','||r_section_rec.ssbsect_crn||','||
									'c_rectype'||','||r_catalog_rec.scrrmaj_rec_type||','||
									'c_major_ind'||','||r_catalog_rec.scrrmaj_major_ind||','||
									'c_majr_code'||','||r_catalog_rec.scrrmaj_majr_code||','||
									'c_lfst_code'||','||r_catalog_rec.scrrmaj_lfst_code||','||
									'date'||','||sysdate);
				    else 				   
				                    UTL_FILE.PUT_LINE(i_lis_file,'       '||'Insert:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
									's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
									's_CRN'||','||r_section_rec.ssbsect_crn||','||
									'c_rectype'||','||r_catalog_rec.scrrmaj_rec_type||','||
									'c_major_ind'||','||r_catalog_rec.scrrmaj_major_ind||','||
									'c_majr_code'||','||r_catalog_rec.scrrmaj_majr_code||','||
									'c_lfst_code'||','||r_catalog_rec.scrrmaj_lfst_code||','||
									'date'||','||sysdate);
							
							  BEGIN        
									  INSERT INTO ssrrmaj
											 (ssrrmaj_term_code,
											  ssrrmaj_crn,
											  ssrrmaj_rec_type,
											  ssrrmaj_major_ind,
											  ssrrmaj_majr_code,
											  ssrrmaj_lfst_code,
											  ssrrmaj_activity_date)
										  VALUES
										  (r_section_rec.ssbsect_term_code,
											  r_section_rec.ssbsect_crn,
											  r_catalog_rec.scrrmaj_rec_type,
											  r_catalog_rec.scrrmaj_major_ind,
											  r_catalog_rec.scrrmaj_majr_code,                        
											  r_catalog_rec.scrrmaj_lfst_code,
											  SYSDATE);
										  EXCEPTION
										  WHEN OTHERS THEN
										  UTL_FILE.PUT_LINE(i_lis_file,' ----- ' || sqlerrm);                   
							   END;
							   EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
						      -- rla for troubleshoot add display of commit or rollback 20201029
							   UTL_FILE.PUT_LINE(i_lis_file,v_aumode);
							   
			   
			        end if; 
					
                ELSIF r_catalog_rec.scrrmaj_rec_type is null THEN
                UTL_FILE.PUT_LINE(i_lis_file,'no data found skip records');
                  
                END IF;      
            END LOOP;

       END LOOP;

END LOOP;
     v_flag := '0';

      -- End of Report Clean up
    UTL_FILE.PUT_LINE(i_lis_file,'MAJOR completed successfully  ');
  
END;
	

----------------------------PROGRAM----------------------------------
-- 9.0 rla 20180925 PROGRAM SCRRPRG, SSRRPRG
---------------------------------------------------------------------

procedure p_program_roll (
	i_lis_file	IN UTL_FILE.FILE_TYPE,
	i_semester		IN VARCHAR2, 	
	i_quarter 		IN VARCHAR2, 
	i_aumode		IN VARCHAR2)
	AS

  v_qtr_term ssbsect.ssbsect_term_code%TYPE;
  v_sem_term ssbsect.ssbsect_term_code%TYPE;
  v_aumode varchar2(9);
  
  v_flag varchar2(1) := '0';
    
    -- loop through terms at least one or more, at most two. {one quarter term, one semester term }
  
 Cursor t_term_res IS
  SELECT a.stvterm_code, 
         a.stvterm_trmt_code
    FROM STVTERM a
   WHERE a.stvterm_code IN (v_qtr_term,v_sem_term)
     and a.stvterm_code not in ('000000','999999');
  
  
  ---------------------------------------------
  CURSOR c_section_res(term_in1 varchar2) IS
          SELECT b.ssbsect_term_code,
                 b.ssbsect_crn,       
                 b.ssbsect_subj_code,
                 b.ssbsect_crse_numb 
            FROM SSBSECT b
           WHERE b.ssbsect_term_code = term_in1
		   --- rla 20201109 [ functional user requested update custom job to omit "PRSH" records  #SR-888163626]
		   AND b.ssbsect_crn not in (select ssrattr_crn from ssrattr s where s.ssrattr_attr_code = 'PRSH'
           AND s.ssrattr_term_code = b.ssbsect_term_code and s.ssrattr_crn = b.ssbsect_crn)
       ORDER BY b.SSBSECT_TERM_CODE,b.SSBSECT_SUBJ_CODE,b.SSBSECT_CRSE_NUMB,b.SSBSECT_CRN;
-----------------------------------------------


  CURSOR c_catalog_res (subj_in varchar2, crse_in varchar2, term_in2 varchar2, trmt_in varchar2) IS
    select a.scrrprg_subj_code,
         a.scrrprg_crse_numb,
		 -- eff term 
         a.scrrprg_term_code_effective,
		 -- from term 
		 a.scrrprg_term_code_effective from_term,
		 -- to term, check to see if from term is equal to_to_term (subtract = 0)  if so make 999999, otherwise use  standard logic. 
    CASE WHEN substr((
             SELECT max(f.scrrprg_term_code_effective)     
             FROM scrrprg f 
             WHERE f.scrrprg_subj_code = a.scrrprg_subj_code 
             AND f.scrrprg_crse_numb = a.scrrprg_crse_numb)-a.scrrprg_term_code_effective,1,6)= '0' 
             --------------
             THEN '999999'
          WHEN substr((
             SELECT max(f.scrrprg_term_code_effective)     
             FROM scrrprg f 
             WHERE f.scrrprg_subj_code = a.scrrprg_subj_code 
             AND f.scrrprg_crse_numb = a.scrrprg_crse_numb)-a.scrrprg_term_code_effective,1,6)<> '0'
             -------------- 
             THEN        (select max(f.scrrprg_term_code_effective) from scrrprg f 
                                   where f.scrrprg_subj_code = a.scrrprg_subj_code 
                                     and f.scrrprg_crse_numb = a.scrrprg_crse_numb)
             END to_term,
          --- 
         a.scrrprg_rec_type,
         a.scrrprg_program_ind,
         a.scrrprg_program,
         a.scrrprg_activity_date
    from scrrprg a
    where a.scrrprg_term_code_effective =  (select max(b.scrrprg_term_code_effective) 
                                        from scrrprg b
                                       where b.scrrprg_term_code_effective = a.scrrprg_term_code_effective 
                                       and b.scrrprg_subj_code = a.scrrprg_subj_code
                                       and b.scrrprg_crse_numb = a.scrrprg_crse_numb
                                       and b.scrrprg_term_code_effective in (select stvterm_code from stvterm 
                                                                              where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)									   
                                     group by b.scrrprg_subj_code, b.scrrprg_crse_numb)
     and (select scbcrky.scbcrky_term_code_end 
        from scbcrky 
       where scbcrky.scbcrky_subj_code = a.scrrprg_subj_code
         and scbcrky.scbcrky_crse_numb = a.scrrprg_crse_numb) = '999999'
        and a.scrrprg_subj_code = subj_in 
        and a.scrrprg_crse_numb = crse_in 
        and a.scrrprg_term_code_effective <= term_in2 
		and a.scrrprg_term_code_effective in (select stvterm_code from stvterm 
                                              where decode(substr(stvterm_code,6,1),'0','Q','5','S','9','') = trmt_in)
   order by a.scrrprg_subj_code, 
            a.scrrprg_crse_numb, 
            a.scrrprg_rec_type;
    
BEGIN

			IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;
  
UTL_FILE.PUT_LINE(i_lis_file,'ROLL PROGRAM RESTRICTIONS'); 
UTL_FILE.PUT_LINE(i_lis_file,' ');  

v_qtr_term := i_quarter;
v_sem_term := i_semester;

FOR t_term_rec IN t_term_res LOOP
     UTL_FILE.PUT_LINE(i_lis_file,'');
     UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_code);
	 	 UTL_FILE.PUT_LINE(i_lis_file,t_term_rec.stvterm_trmt_code);
     UTL_FILE.PUT_LINE(i_lis_file,'');
     
     delete from ssrrprg where ssrrprg_term_code = t_term_rec.stvterm_code;
	 EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
     

       FOR r_section_rec IN c_section_res(t_term_rec.stvterm_code) LOOP    
         UTL_FILE.PUT_LINE(i_lis_file,r_section_rec.ssbsect_term_code||','||r_section_rec.ssbsect_subj_code||','||r_section_rec.ssbsect_crse_numb||','||r_section_rec.ssbsect_crn);
              
            FOR r_catalog_rec in c_catalog_res(r_section_rec.ssbsect_subj_code,r_section_rec.ssbsect_crse_numb,r_section_rec.ssbsect_term_code, t_term_rec.stvterm_trmt_code) LOOP

               IF r_catalog_rec.scrrprg_rec_type is not null 			 
			   THEN
               v_flag := '1';
	   
	   
				  if r_catalog_rec.to_term <= t_term_rec.stvterm_code 
				  then 
					UTL_FILE.PUT_LINE(i_lis_file,'SKIP RECORD:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||     
							's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
							's_CRN'||','||r_section_rec.ssbsect_crn||','||
							'c_rectype'||','||r_catalog_rec.scrrprg_rec_type||','||
							'c_program_ind'||','||r_catalog_rec.scrrprg_program_ind||','||
							'c_program'||','||r_catalog_rec.scrrprg_program||','||
							'date'||','||sysdate);
							
				   else 
							 UTL_FILE.PUT_LINE(i_lis_file,'     '||'Inserting:'||' '||'from term'||','||r_catalog_rec.from_term||','||'to_term'||','||r_catalog_rec.to_term||','||
							's_TERM_CODE'||','||r_section_rec.ssbsect_term_code||','||
							's_CRN'||','||r_section_rec.ssbsect_crn||','||
							'c_rectype'||','||r_catalog_rec.scrrprg_rec_type||','||
							'c_program_ind'||','||r_catalog_rec.scrrprg_program_ind||','||
							'c_program'||','||r_catalog_rec.scrrprg_program||','||
							'date'||','||sysdate);
		  
							  BEGIN
										
										INSERT INTO saturn.ssrrprg(
												ssrrprg_term_code,
												ssrrprg_crn,
												ssrrprg_rec_type,
												ssrrprg_program_ind,
												ssrrprg_program,
												ssrrprg_activity_date)
											VALUES
											(r_section_rec.ssbsect_term_code,
												r_section_rec.ssbsect_crn,
												r_catalog_rec.scrrprg_rec_type,
												r_catalog_rec.scrrprg_program_ind,
												r_catalog_rec.scrrprg_program,
												sysdate);
											EXCEPTION
											WHEN OTHERS THEN
											UTL_FILE.PUT_LINE(i_lis_file,' ----- ' || sqlerrm); 
							  END;
							  EXECUTE IMMEDIATE ('begin '||v_aumode||' end;');
															
					end if;
			 
              ELSIF  r_catalog_rec.scrrprg_rec_type is null THEN
              UTL_FILE.PUT_LINE(i_lis_file,'no data found skip records');
               
              END IF;  
	  
            END LOOP;
       END LOOP;
END LOOP;
v_flag := '0';

      -- End of Report Clean up
    UTL_FILE.PUT_LINE(i_lis_file,' PROGRAM completed successfully  ');
  
END;
---------------------------------------------------------------------------------------------
-------------------------
-- MAIN 
-------------------------

procedure p_main(
      i_lis_filename 	IN VARCHAR2,
	  i_semester 		IN VARCHAR2,
	  i_quarter 		IN VARCHAR2,
	  i_aumode 		IN VARCHAR2)
AS

	-- Open .lis output file	

	v_lis_file UTL_FILE.FILE_TYPE := UTL_FILE.fopen(szprstr.c_feed_dir, i_lis_filename, 'W');
	
	v_aumode varchar2(9);
			   
		BEGIN 
		
		    IF i_aumode = 'U' THEN
					v_aumode := 'COMMIT;';
			ELSE 
					v_aumode := 'ROLLBACK;';
			END IF;
		
		     -- LEAVE AS DBMS OUTPUT FOR LOG FILE
		    dbms_output.put_line(SYSDATE);
				
			-- Report Header
			UTL_FILE.PUT_LINE(v_lis_file, ewuapp.f_report_header128(' ', 'Eastern Washington University', ''));
			UTL_FILE.PUT_LINE(v_lis_file, ewuapp.f_report_header128(' ', 'Eastern Washington University', ''));
			UTL_FILE.PUT_LINE(v_lis_file, ewuapp.f_report_header128(' ', 'Restrictions Roll (SZPRSTR)', ''));
			UTL_FILE.PUT_LINE(v_lis_file, ewuapp.f_report_header128(' ', 'Run at ' || to_char(SYSDATE, 'HH:MMAM DD-MON-YYYY'), ''));
			UTL_FILE.PUT_LINE(v_lis_file, ' ');
			
			
			-- Run Procedures(s)
			
			p_class_roll(v_lis_file, i_semester, i_quarter, i_aumode);
			p_degree_roll(v_lis_file, i_semester, i_quarter, i_aumode);
			p_department_roll(v_lis_file, i_semester, i_quarter, i_aumode);
			p_level_roll(v_lis_file, i_semester, i_quarter, i_aumode);
			p_major_roll(v_lis_file, i_semester, i_quarter, i_aumode);
			p_program_roll(v_lis_file, i_semester, i_quarter, i_aumode);
			
				-- Clean up
		     UTL_FILE.PUT_LINE(v_lis_file, ' ');
			UTL_FILE.fclose(v_lis_file);

		END;-- p_main;

END SZPRSTR;
/
show errors;
/
