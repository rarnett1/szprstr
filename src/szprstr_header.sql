--20180925
create or replace PACKAGE EWUAPP.SZPRSTR AS
  
   --SZPRSTR - ROLL RESTRICTIONS {class, degree, department, level, major, program}
   --Banner Custom Job
   
  c_feed_dir  constant varchar(20) := 'HR_DIR';
  -- linda says c_feed_dir  constant varchar(20) := 'RR_DIR';
  
   --Entry point for custom process SZPRSTR

    procedure p_main(
      i_lis_filename 	IN VARCHAR2,
	  i_semester 		IN VARCHAR2,
	  i_quarter 		IN VARCHAR2,
	  i_aumode 		IN VARCHAR2);

END SZPRSTR;
/
commit;
/