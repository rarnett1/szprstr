--------------------------------------------------------------------------------
-- Define parameters for job SZPRSTR
--------------------------------------------------------------------------------

-- Clear previous parameters
DELETE FROM GJBPDEF WHERE gjbpdef_job = 'SZPRSTR';

-- Parameter {"%01d" % p_num}: SAE BATCH ID
insert into GJBPDEF (
    gjbpdef_job,
    gjbpdef_number,
    gjbpdef_desc,
    gjbpdef_length,
    gjbpdef_type_ind,
    gjbpdef_optional_ind,
    gjbpdef_single_ind,
    gjbpdef_activity_date,
	gjbpdef_validation,
	gjbpdef_list_values,
    gjbpdef_help_text
    )
values (
    'SZPRSTR', -- gjbpdef_job
    '01', -- gjbpdef_number
    'SEMESTER', -- gjbpdef_desc
    6, -- gjbpdef_length
    'C', -- gjbpdef_type_ind
    'R', -- gjbpdef_optional_ind
    'S', -- gjbpdef_single_ind
    sysdate, -- gjbpdef_activity_date
	'STVTERM_EQUAL',
	'STVTERM',
    'Enter semester term'
);

commit;

-- Parameter {"%02d" % p_num}: BATCH_DATE
insert into GJBPDEF (
    gjbpdef_job,
    gjbpdef_number,
    gjbpdef_desc,
    gjbpdef_length,
    gjbpdef_type_ind,
    gjbpdef_optional_ind,
    gjbpdef_single_ind,
    gjbpdef_activity_date,
    gjbpdef_validation,
	gjbpdef_list_values,
    gjbpdef_help_text
    )
values (
    'SZPRSTR', -- gjbpdef_job
    '02', -- gjbpdef_number
    'QUARTER', -- gjbpdef_desc
    6, -- gjbpdef_length
    'C', -- gjbpdef_type_ind
    'R', -- gjbpdef_optional_ind
    'S', -- gjbpdef_single_ind
    sysdate, -- gjbpdef_activity_date
	'STVTERM_EQUAL',
	'STVTERM',
    'Enter quarter term '
);

commit;

-- Parameter {"%03d" % p_num}: EMPLOYEE ID 
insert into GJBPDEF (
    gjbpdef_job,
    gjbpdef_number,
    gjbpdef_desc,
    gjbpdef_length,
    gjbpdef_type_ind,
    gjbpdef_optional_ind,
    gjbpdef_single_ind,
    gjbpdef_activity_date,
    gjbpdef_help_text
    )
values (
    'SZPRSTR', -- gjbpdef_job
    '03', -- gjbpdef_number
    'AUMODE', -- gjbpdef_desc
    1, -- gjbpdef_length
    'C', -- gjbpdef_type_ind
    'R', -- gjbpdef_optional_ind
    'S', -- gjbpdef_single_ind
    sysdate, -- gjbpdef_activity_date
    'Enter (A)udit or (U)pdate'
);

commit;

